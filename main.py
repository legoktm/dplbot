"""
Rewrite of DynamicPageList (Wikimedia) aka Intersection as a bot
Copyright (C) https://en.wikinews.org/wiki/User:Amgine
Copyright (C) https://en.wikinews.org/wiki/User:IlyaHaykinson
Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
import re

from typing import Optional

import mwparserfromhell


MAX_CATEGORIES = 6  # $wgDLPmaxCategories
MAX_RESULT_COUNT = 200  # $wgDLPMaxResultCount


def preprocess(text) -> str:
    # TODO: implement
    return text


def validate_title(text) -> Optional[str]:
    # TODO: implement
    return text


def validate_namespace(ns) -> Optional[int]:
    # TODO: implement
    return int(ns)


def handle_page(text: str):
    code = mwparserfromhell.parse(text)
    for template in code.filter_templates():
        if template.name == "DPL":
            handle_template(template)


def php_intval(val) -> int:
    try:
        return int(val)
    except ValueError:
        return 0


def is_falsey_str(val: str) -> bool:
    if val == "no" or val == "false":
        return False
    else:
        return True


def extension_loaded(name: str) -> bool:
    # TODO: implement
    return True


def mysql_quote(text: str) -> str:
    # FIXME: implement
    return text


def db_key(text: str) -> str:
    return text.split(":", 1)[-1].replace(" ", "_")


def handle_template(template: mwparserfromhell.nodes.template.Template):
    categories = []
    if template.has("category"):
        cat = validate_title("Category:" + preprocess(template.get("category").value))
        if cat is not None:
            categories.append(cat)

    exclude_categories = []
    if template.has("notcategory"):
        cat = validate_title("Category:" + preprocess(template.get("notcategory").value))
        if cat is not None:
            exclude_categories.append(cat)

    namespace_index = 0
    namespace_filtering = False
    if template.has("namespace"):
        ns = validate_namespace(template.get("namespace").value)
        if ns is not None:
            namespace_index = ns
            namespace_filtering = True
        else:
            """
            // Note, since intval("some string") = 0
            // this considers pretty much anything
            // invalid here as the main namespace.
            // This was probably originally a bug,
            // but is now depended upon by people
            // writing things like namespace=main
            // so be careful when changing this code.
            $namespaceIndex = intval( $arg );
            $namespaceFiltering = ( $namespaceIndex >= 0 );
            """
            # Ummm is this right??
            namespace_index = php_intval(template.get("namespace").value)
            namespace_filtering = True

    count = 0
    count_set = False
    if template.has("count"):
        try:
            count = int(template.get("count").value)
            count_set = True
        except ValueError:
            pass

    offset = 0
    if template.has("offset"):
        offset = php_intval(template.get("offset").value)

    if template.has("imagewidth"):
        gallery_image_width = php_intval(template.get("imagewidth").value)
    if template.has("imageheight"):
        gallery_image_width = php_intval(template.get("imagewidth").value)
    if template.has("imagesperrow"):  # images per row
        gallery_numb_rows = php_intval(template.get("imagesperrow").value)

    # TODO: mode
    gallery_caption = None
    if template.has("gallerycaption"):
        gallery_caption = preprocess(template.get("gallerycaption").value)

    gallery_file_size = False
    if template.has("galleryshowfilesize"):
        gallery_file_size = is_falsey_str(template.get("galleryshowfilesize").value)

    gallery_file_name = False
    if template.has("galleryshowfilename"):
        gallery_file_name = is_falsey_str(template.get("galleryshowfilename").value)

    order = 'DESC'
    if template.has("order"):
        if template.get("order").value == "ascending":
            order = "ASC"
        else:
            order = "DESC"

    order_method = "categoryadd"
    if template.has("ordermethod"):
        val = template.get("ordermethod").value
        if val == "sortkey":
            val = "categorysortkey"
        valid = ("lastedit", "length", "created", "categorysortkey", "categoryadd")
        if val in valid:
            order_method = val
        else:
            order_method = "categoryadd"

    redirects = "exclude"
    if template.has("redirects"):
        val = template.get("redireccts").value
        if val in ("include", "only", "exclude"):
            redirects = val
        else:
            redirects = "exclude"

    stable = "include"
    flagged_revs = False
    if template.has("stablepages"):
        val = template.get("stablepages").value
        if val == "include":
            stable = "include"
        elif val == "only":
            flagged_revs = True
            stable = "only"
        else:
            flagged_revs = True
            stable = "exclude"

    quality = "include"
    if template.has("qualitypages"):
        val = template.get("qualitypages").value
        if val == "include":
            quality = "include"
        elif val == "only":
            flagged_revs = True
            quality = "only"
        else:
            flagged_revs = True
            quality = "exclude"

    add_first_category_date = False
    date_format = ''
    strip_year = False
    if template.has("addfirstcategorydate"):
        val = template.get("addfirstcategorydate").value
        match = re.match('([ymd]{2,3}|ISO 8601)$', val)
        if val == "true":
            add_first_category_date = True
        elif match:
            # if it more or less is valid dateformat.
            add_first_category_date = True
            date_format = val
            if len(date_format) == 2:
                # DateFormatter does not support no year. work around
                date_format += 'y'
                strip_year = True
        else:
            add_first_category_date = False

    show_namespace = True
    if template.has("shownamespace"):
        show_namespace = template.get("shownamespace").value != "false"

    ignore_subpages = False
    if template.has("ignoresubpages"):
        ignore_subpages = template.get("ignoresubpages").value == "true"

    # XXX: googlehack is obsolete since 2015
    # XXX: cannot support nofollow as a bot

    cat_count = len(categories)
    exclude_cat_count = len(exclude_categories)
    total_cat_count = cat_count + exclude_cat_count

    if cat_count < 1 and not namespace_filtering:
        # TODO: output "no included categories" error
        return

    if total_cat_count > MAX_CATEGORIES:
        # TODO: output "too many categories" error
        return

    if count_set:
        if count < 1:
            count = 1
        if count > MAX_RESULT_COUNT:
            count = MAX_RESULT_COUNT

    # disallow showing date if the query doesn't have an inclusion category parameter
    if cat_count < 1:
        add_first_category_date = False
        # don't sort by fields relating to categories if there are no categories.
        if order_method in ('categoryadd', 'categorysortkey'):
            order_method = 'created'

    # build the SQL query
    tables = ['page']
    fields = ['page_namespace', 'page_title']
    where = {}
    join = {}
    options = {}

    if add_first_category_date:
        fields.append("c1.cl_timestamp")

    if namespace_filtering:
        where['page_namespace'] = namespace_index

    if flagged_revs and extension_loaded('FlaggedRevs'):
        tables.append('flaggedpages')
        join['flaggedpages'] = ['LEFT JOIN', 'page_id = fp_page_id']
        if stable == "only":
            where['PLACEHOLDER1'] = 'fp_stable IS NOT NULL'
        elif stable == "exclude":
            where["fp_stable"] = None

        if quality == "only":
            where["PLACEHOLDER2"] = 'fp_quality >= 1'
        elif quality == "exclude":
            where["PLACEHOLDER3"] = "fp_quality = 0 OR fp_quality IS NULL"

    if redirects == "only":
        where["page_is_redirect"] = 1
    elif redirects == "exclude":
        where["page_is_redirect"] = 0

    if ignore_subpages:
        where["PLACEHOLDER4"] = "page_title NOT LIKE '%/%'"

    # TODO: gallery

    # Alias each category as c1, c2, etc.
    current_table_number = 1
    for cat in categories:
        join[f"c{current_table_number}"] = [
            "INNER JOIN",
            [
                f"page_id = c{current_table_number}.cl_from",
                f"c{current_table_number}.cl_to={mysql_quote(db_key(cat))}"
            ]
        ]
        tables.append(f"categorylinks AS c{current_table_number}")
        current_table_number += 1

    for cat in exclude_categories:
        join[f"c{current_table_number}"] = [
            "LEFT OUTER JOIN",
            [
                f"page_id = c{current_table_number}.cl_from",
                f"c{current_table_number}.cl_to={mysql_quote(db_key(cat))}"
            ]
        ]
        tables.append(f"categorylinks AS c{current_table_number}")
        where[f"c{current_table_number}.cl_to"] = None
        current_table_number += 1

    sort_by = {
        "lastedit": "page_touched",
        "length": "page_len",
        "created": "page_id",
        "categorysortkey": f"c1.cl_type {order}, c1.cl_sortkey",
        "categoryadd": "c1.cl_timestamp",
    }[order_method]

    options["ORDER BY"] = f"{order} {sort_by}"

    if count_set:
        options["LIMIT"] = count

    if offset > 0:
        options["OFFSET"] = offset

    # TODO: excude the query
